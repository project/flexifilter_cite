
This module is designed to work with the flexifilter module (http://drupal.org/project/flexifilter).

It can be used as an example of a module that invokes flexifilter hooks and makes use of the flexifilter API.