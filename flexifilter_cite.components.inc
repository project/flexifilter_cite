<?php

/**
 * Implementation of hook_flexifilter_components()
 */
function flexifilter_cite_flexifilter_components() {
  $components = array();
  $components['flexifilter_cite_component_nid'] = array(
    'label' => t('Cite by node id.'),
    'description' => t('Cites a node by node id. Ugly for user interface, but sidesteps problems for multiple nodes with the same title.'),
    'callback' => 'flexifilter_cite_component_nid',
    'group' => t('Node citations'),
    'step' => 'either',
  );
  $components['flexifilter_cite_component_title'] = array(
    'label' => t('Cite by node title.'),
    'description' => t('Cites a node by title. Pretty for user interface, but may have problems for multiple nodes with the same title.'),
    'callback' => 'flexifilter_cite_component_title',
    'group' => t('Node citations'),
    'step' => 'either',
  );
  return $components;
}

/**
 * Flexifilter component callback.
 * Handles a simple text replacement through str_replace.
 */
function flexifilter_cite_component_nid($op, $settings, $text = '') {
  switch ($op) {
    case 'settings':
      $form = array();
      $form['max'] = array(
        '#type' => 'textfield',
        '#title' => t('Maximum number of nodes to cite. Set to 0 for infinite.'),
        '#default_value' => isset($settings['max']) ? $settings['max'] : '0',
        '#required' => TRUE,
      );
      $form['type'] = array(
        '#type' => 'radios',
        '#title' => t('Type of citation'),
        '#options' => array(
          0 => t('Link'),
          1 => t('Full body'),
          2 => t('Teaser'),
          3 => t('Body'),
        ),
        '#default_value' => isset($settings['type']) ? $settings['type'] : 2,
        '#required' => TRUE,
      );
      return $form;

    case 'prepare':
    case 'process':
      $exploded = explode(' ', $text);
      $max = isset($settings['max']) ? $settings['max'] : '0';
      if ($max == 0) {
        $max = 'infinite';
      }
      foreach ($exploded as &$word) {
        if ($max != 0) {
          if (is_numeric($word)) {
            $word = flexifilter_cite_nid_replacement($word, $settings['type']);
            if ($max != 'infinite') {
              $max--;
            }
          }
        }
      }
      return implode(' ', $exploded);
  }
  return $text;
}

/**
 * Helper function for flexifilter_cite_component_nid().
 * Returns the replacement for a node id (numeric).
 * Runs an access check on teaser & body, but links are free.
 */
function flexifilter_cite_nid_replacement($word, $type) {
  switch ($type) {
    case 0:
      return l($word, "node/$word");
    case 1:
      $node = node_load($word);
      if (node_access('view', $node)) {
        return node_view($node);
      }
      return $word;
    case 2:
      $node = node_load($word);
      if (node_access('view', $node)) {
        return node_teaser($node->body);
      }
      return $word;
    case 3:
      $node = node_load($word);
      if (node_access('view', $node)) {
        return $node->body;
      }
      return $word;
    default:
      return $word;
  }
  // Highly unlikely we're getting here.
  return $word;
}

/**
 * Flexifilter component callback.
 * Handles a simple text replacement through str_replace.
 */
function flexifilter_cite_component_title($op, $settings, $text = '') {
  switch ($op) {
    case 'settings':
      $form = array();
      $form['max'] = array(
        '#type' => 'textfield',
        '#title' => t('Maximum number of nodes to cite. Set to 0 for infinite.'),
        '#default_value' => isset($settings['max']) ? $settings['max'] : '0',
        '#required' => TRUE,
      );
      $form['type'] = array(
        '#type' => 'radios',
        '#title' => t('Type of citation'),
        '#options' => array(
          0 => t('Link'),
          1 => t('Full body'),
          2 => t('Teaser'),
          3 => t('Body'),
        ),
        '#default_value' => isset($settings['type']) ? $settings['type'] : 2,
        '#required' => TRUE,
      );
      return $form;

    case 'prepare':
    case 'process':
      $exploded = explode(' ', $text);
      $max = isset($settings['max']) ? $settings['max'] : '0';
      if ($max == 0) {
        $max = 'infinite';
      }
      foreach ($exploded as &$word) {
        if ($max > 0 || $max == 'infinite') {
          if (is_object($node = node_load(array('title' => $word)))) {
            $word = flexifilter_cite_title_replacement($word, $node, $settings['type']);
            if ($max != 'infinite') {
              $max--;
            }
          }
        }
      }
      return implode(' ', $exploded);
  }
  return $text;
}

/**
 * Helper function for flexifilter_cite_component_nid().
 * Returns the replacement for a node id (numeric).
 * Runs an access check on teaser & body, but links are free.
 */
function flexifilter_cite_title_replacement($word, $node, $type) {
  switch ($type) {
    case 0:
      return l($word, "node/". $node->nid);
    case 1:
      if (node_access('view', $node)) {
        return node_view($node);
      }
      return $word;
    case 2:
      if (node_access('view', $node)) {
        return node_teaser($node->body);
      }
      return $word;
    case 3:
      if (node_access('view', $node)) {
        return $node->body;
      }
      return $word;
    default:
      return $word;
  }
  // Highly unlikely we're getting here.
  return $word;
}