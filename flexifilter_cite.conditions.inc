<?php

/**
 * Implementation of hook_flexifilter_conditions()
 */
function flexifilter_cite_flexifilter_conditions() {
  $conditions = array();
  $conditions['flexifilter_cite_is_node_title'] = array(
    'label' => t('OR Group'),
    'description' => t('True if the string passed in is a node title.'),
    'callback' => 'flexifilter_cite_condition_is_title',
    'group' => t('Node citations'),
  );
  return $conditions;
}

/**
 * Flexifilter condition callback.
 * Returns TRUE if the text passed in is a node title.
 */
function flexifilter_cite_condition_is_title($op, $settings, $text) {
  switch ($op) {
    case 'settings':
      $form = array();
      return $form;

    case 'prepare':
    case 'process':
      return is_object(node_load(array('title' => $text)));
  }
}